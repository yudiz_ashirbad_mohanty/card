import React from 'react'
import './Card.css';

const Card = ({data}) => {
    console.log(data)
    const { sport_event, sport_event_status, statistics } = data;
  return (
    <div className='main-card'>
    <div className='sportevent'>
      <div>
        <span className='name'>{sport_event.season.name}</span>
        <p className='vs'>
          {sport_event.competitors[0].name} VS{" "}  {sport_event.competitors[1].name}
        </p>
      </div>

      <div className='venue'>
        <p>
          {sport_event.venue.name}, {sport_event.venue.city_name} |
        </p>
        <p className='result'>{sport_event_status.match_result}</p>
      </div>
    </div>

    <div>
      <div>
        <div className='1'>
          <span className="run">
            {sport_event_status.period_scores[0].display_score}{" "}{sport_event_status.period_scores[0].display_overs} OV
          </span>
        </div>
        <div className="2">
          <div>
           
          </div>
          <span className="run">
            {sport_event_status.period_scores[1].display_score}{" "}
            {sport_event_status.period_scores[1].display_overs} OV
          </span>
        </div>
      </div>

      <div className="statistics">
        <img
          src={statistics.man_of_the_match[0].img}
          alt={statistics.man_of_the_match[0].name}
        />
        <div className="heading">
          <span className="title">PLAYER OF THE MATCH</span>
          <span className="player-name">
            {statistics.man_of_the_match[0].name}
          </span>
        </div>
      </div>
    </div>
  </div>

  )
}

export default Card