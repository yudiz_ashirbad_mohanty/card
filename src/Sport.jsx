import React from 'react'
import Card from './Card'
import data from './data.json' 
const Sport = () => {
  return (
      <div>
         {
        data.map((sport,index)=>{
            return(
                <div key={index}>
               <Card data={sport.data} />
                </div>
            )
        })
         }
         
      </div>
  )
}

export default Sport
